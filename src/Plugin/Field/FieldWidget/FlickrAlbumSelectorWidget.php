<?php

/**
 * @file
 * Contains \Drupal\flickr_album_field\Plugin\Field\FieldWidget\FlickrAlbumSelectorWidget.
 */

namespace Drupal\flickr_album_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'flickr_album_selector' widget.
 *
 * @FieldWidget(
 *   id = "flickr_album_selector",
 *   label = @Translation("Flickr Album Selector"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class FlickrAlbumSelectorWidget extends WidgetBase {

  /**
   * @var string
   */
  private $api_format = 'json';

  /**
   * @var string
   */
  private $api_endpoint = 'https://api.flickr.com/services/rest/';

  /**
   * @var string
   */
  private $api_method_album = 'flickr.photosets.getList';


  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : NULL;
    $element['value'] = $element + array(
      '#type' => 'textfield',
      '#default_value' => $value,
    );

    return $element;
  }

  /**
   * Returns all avaibale options by calling the Flickr API.
   * @return array
   */
  /*protected function getOptions() {

    $json_response = array();
    $options = array();

    // Get field settings
    $api_key = $this->getSetting('flickr_api_key');
    $user_id = $this->getSetting('flickr_user_id');

    if(empty($api_key) || empty($user_id)) {
      drupal_set_message($this->t('You need to set User ID and API Key fields in field settings!'), 'error');
    }

    // Call Flickr API
    $client = \Drupal::httpClient();

    try {
      $get_request = $client->createRequest('GET', $this->api_endpoint);
      $get_request->getQuery()->set('method', $this->api_method_album);
      $get_request->getQuery()->set('api_key', $api_key);
      $get_request->getQuery()->set('user_id', $user_id);
      $get_request->getQuery()->set('format', $this->api_format);
      $get_request->getQuery()->set('nojsoncallback', 1);

      $get_response = $client->send($get_request);

      // Check if request was successfully
      if($get_response->getStatusCode() != 200) {

        drupal_set_message(
          $this->t(
            'Error, getting albums from Flickr: @error',
            array('@error' => (string)$get_response)
          ), 'error');

      } else {
        $json_response = $get_response->json();
      }
    } catch (\Exception $e) {
      drupal_set_message(
        $this->t(
          'Error, connecting to Flickr: @error',
          array('@error' => $e->getMessage())
        ), 'error');
    }

    if(empty($json_response)) {
      drupal_set_message($this->t('Got no response from Flickr!'), 'error');
      return array();
    }

    if($json_response['stat'] == 'fail') {
      drupal_set_message(
        $this->t(
          'Error, getting albums from Flickr: @error!',
          array('@error' => $json_response['message'])
        ),
        'error'
      );

    // Add options
    } else {
      foreach($json_response['photosets']['photoset'] as $album) {
        $options[$album['id']] = format_date($album['date_create']) . ' - ' . $album['title']['_content'];
      }
    }

    return $options;
  }*/

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'flickr_api_key' => '',
      'flickr_user_id' => '',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    // Settings form elements
    $elements = parent::settingsForm($form, $form_state);

    // Flickr API Key field
    $elements['flickr_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Flickr API Key'),
      '#default_value' => $this->getSetting('flickr_api_key'),
      '#required' => TRUE,
      '#description' => $this->t('You need to <a href="https://www.flickr.com/services/apps/create/apply">create an Flickr app</a> to get an API Key.'),
    );

    // Flickr User ID field
    $elements['flickr_user_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Flickr User ID'),
      '#default_value' => $this->getSetting('flickr_user_id'),
      '#required' => TRUE,
      '#description' => $this->t('You can select albums from this user. <a href="http://idgettr.com/">Find out your User ID</a>.'),
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = array();
    $user_id = $this->getSetting('flickr_user_id');

    if (empty($user_id)) {
      $summary[] = $this->t('You need to set an User ID!');
    }
    else {
      $summary[] = $this->t('Albums from User: @user_id', array('@user_id' => $user_id));
    }

    return $summary;
  }
}
